import java.util.ArrayList;

public class Postfix
{
    public double eval(String expr)
    {
        String[] tokens = expr.split(" ");
        return eval(tokens);
    }
        
    public double eval(ArrayList<String> tokens)
    {
        return eval(tokens.toArray(new String[tokens.size()]));
    }

    public double eval(String[] tokens)
    {
        Stack<Double> s = new Stack<Double>();
        try
        {
            for (int i = 0; i < tokens.length; i++)
            {
                String token = tokens[i];
                
                switch(token)
                {
                    case "+":
                        {
                            double a = s.pop();
                            double b = s.pop();
                            s.push(b + a);
                            break;
                        }
                    case "*":
                        {
                            double a = s.pop();
                            double b = s.pop();
                            s.push(b * a);
                            break;
                        }
                    case "-":
                        {
                            double a = s.pop();
                            double b = s.pop();
                            s.push(b - a);
                            break;
                        }
                    case "/":
                        {
                            double a = s.pop();
                            double b = s.pop();
                            s.push(b / a);
                            break;
                        }
                    case "^":
                        {
                            double a = s.pop();
                            double b = s.pop();
                            s.push(Math.pow(b, a));
                            break;
                        }
                    case "pi":
                    case "Pi":
                    case "PI":
                        {
                            s.push(Math.PI);
                            break;
                        }
                    case "tan":
                    case "Tan":
                    case "TAN":
                        {
                            double a = s.pop();
                            s.push(Math.tan(a));
                            break;
                        }
                    case "cos":
                    case "Cos":
                    case "COS":
                        {
                            double a = s.pop();
                            s.push(Math.cos(a));
                            break;
                        }
                    case "sin":
                    case "Sin":
                    case "SIN":
                        {
                            double a = s.pop();
                            s.push(Math.sin(a));
                            break;
                        }
                    
                    default:
                        s.push(Double.parseDouble(token));
                        break;
                    
                }
    
            }
        
        return s.peek();
        
        }
        catch (java.lang.NumberFormatException ioobe)
        {
                throw new InvalidTokenException();
        }
    }
}