import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class SYTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class SYTest
{
    /**
     * Default constructor for test class SYTest
     */
    public SYTest()
    {
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp()
    {
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown()
    {
    }
    
    /*@Test
    public void testConvert()
    {
        SY s = new SY();
        assertEquals("[2, 3, +]", s.convert("2 + 3").toString());
        assertEquals("[2, 3, ^, 4, ^, 5, 6, ^, 7, ^, +]", s.convert("2 ^ 3 ^ 4 + 5 ^ 6 ^ 7").toString());
    }
    */
    @Test
    public void testConvertParentheses()
    {
        SY s = new SY();
        assertEquals("[5, 4, +, 3, *]", s.convert("( 5 + 4 ) * 3").toString());
    }
    @Test
    public void testConvertParentheses2()
    {
        SY s = new SY();
        assertEquals("[5, 2, 3, +, *]", s.convert("5 * ( 2 + 3 )").toString());
    }
    @Test
    public void testConvertParentheses3()
    {
        SY s = new SY();
        assertEquals("[3, 5, *, 2, 5, 3, +, /, +]", s.convert("3 * 5 + 2 / ( 5 + 3 )").toString());
    }
    @Test
    public void testRightAsso()
    {
        SY s = new SY();
        assertEquals("[4, 2, 3, ^, ^]", s.convert("4 ^ 2 ^ 3").toString());
    }
    
}