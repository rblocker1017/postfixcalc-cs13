import acm.program.Program;
import acm.graphics.*;      // GCanvas
import javax.swing.*;       // JButton, JTextField, Jwhatever
import java.awt.event.*;    // Event
import java.awt.*;          // Font, colors
import java.util.ArrayList;
import java.util.HashMap;

public class PostfixView extends Program
{
    private JTextField infixField;
    private JTextField postfixField;
    private JTextField resultField;
    
    public PostfixView()
    {
        start();
        setSize(500, 500);
    }
    
    public void init()
    {
        GCanvas canvas = new GCanvas();
        add(canvas);
        
        JLabel infixLabel = new JLabel("Infix Expression");
        JLabel postfixLabel = new JLabel("Postfix Expression");
        JLabel resultLabel = new JLabel("Result");
        infixField = new JTextField();
        postfixField = new JTextField();
        resultField = new JTextField();
        JButton calcButton = new JButton("Calculate");
        calcButton.setActionCommand("Calculate");
        JButton clearButton = new JButton("Clear");
        
        
        // number buttons start
        JButton oneButton = new JButton("1");
        JButton twoButton = new JButton("2");
        JButton threeButton = new JButton("3");
        JButton fourButton = new JButton("4");
        JButton fiveButton = new JButton("5");
        JButton sixButton = new JButton("6");
        JButton sevenButton = new JButton("7");
        JButton eightButton = new JButton("8");
        JButton nineButton = new JButton("9");
        JButton zeroButton = new JButton("0");
        // number buttons end
        
        // operator buttons start
        JButton addButton = new JButton("+");
        JButton subButton = new JButton("-");
        JButton multButton = new JButton("*");
        JButton divideButton = new JButton("/");
        JButton exponentButton = new JButton("^");
        JButton openingParaButton = new JButton("(");
        JButton closingParaButton = new JButton(")");
        // operator buttons end
        
        // trig buttons start
        JButton piButton = new JButton("Pi");
        JButton tanButotn = new JButton("Tan");
        JButton cosButton = new JButton("Cos");
        JButton sinButton = new JButton("Sin");
        // trig buttons end
        
        
        Font f0 = new Font("Helvetica", Font.BOLD, 12);
        infixLabel.setFont(f0);
        postfixLabel.setFont(f0);
        resultLabel.setFont(f0);
        
        canvas.setBackground(Color.WHITE);
        
        infixField.setSize(150, 20);
        postfixField.setSize(150, 20);
        resultField.setSize(150, 20);
        
        canvas.add(infixLabel, 50, 50);
        canvas.add(postfixLabel, 50, 100);
        canvas.add(resultLabel, 50, 150);
        canvas.add(infixField, 175, 50);
        canvas.add(postfixField, 175, 100);
        canvas.add(resultField, 175, 150);
        canvas.add(calcButton, 50, 200);
        canvas.add(clearButton, 175, 200);
        
        canvas.add(oneButton, 50, 250);
        canvas.add(twoButton, 100, 250);
        canvas.add(threeButton, 150, 250);
        canvas.add(fourButton, 200, 250);
        canvas.add(fiveButton, 250, 250);
        canvas.add(sixButton, 50, 300);
        canvas.add(sevenButton, 100, 300);
        canvas.add(eightButton, 150, 300);
        canvas.add(nineButton, 200, 300);
        canvas.add(zeroButton, 250, 300);
        
        canvas.add(exponentButton, 50, 350);
        canvas.add(openingParaButton, 100, 350);
        canvas.add(closingParaButton, 150, 350);
        
        canvas.add(piButton, 200, 350);
        canvas.add(tanButotn, 258, 350);
        canvas.add(cosButton, 323, 350);
        canvas.add(sinButton, 388, 350);
        
        canvas.add(addButton, 300, 250);
        canvas.add(subButton, 350, 250);
        canvas.add(multButton, 300, 300);
        canvas.add(divideButton, 350, 300);
      
        
        
        addActionListeners();
    }
    
    public void actionPerformed(ActionEvent ae)
    {
        String whichButton = ae.getActionCommand();
        
        switch(whichButton)
        {
            case "Calculate":
            if(!infixField.getText().equals("")) // checks if there is an infix Expression first
            {
                SY s = new SY();
                ArrayList<String> postfixExp = new ArrayList<String>();
                String infixExp = infixField.getText();
                postfixExp = s.convert(infixExp);
                postfixField.setText(postfixExp.toString());
                
                Postfix p = new Postfix();
                String postfixExp2 = postfixField.getText();
                double result = 0;
                result = p.eval(postfixExp);
                resultField.setText(result + "");
            }
            
            else if(!postfixField.getText().equals(""))
            {
                Postfix p = new Postfix();
                String postfixExp = postfixField.getText();
                double result = 0;
                result = p.eval(postfixExp);
                resultField.setText(result + "");
            }
            break;
            
            case "Clear":
            infixField.setText("");
            postfixField.setText("");
            resultField.setText("");
            break;
            
            case "1":
            infixField.setText(infixField.getText() + "1");
            break;
            case "2":
            infixField.setText(infixField.getText() + "2");
            break;
            case "3":
            infixField.setText(infixField.getText() + "3");
            break;
            case "4":
            infixField.setText(infixField.getText() + "4");
            break;
            case "5":
            infixField.setText(infixField.getText() + "5");
            break;
            case "6":
            infixField.setText(infixField.getText() + "6");
            break;
            case "7":
            infixField.setText(infixField.getText() + "7");
            break;
            case "8":
            infixField.setText(infixField.getText() + "8");
            break;
            case "9":
            infixField.setText(infixField.getText() + "9");
            break;
            case "0":
            infixField.setText(infixField.getText() + "0");
            break;

            case "+":
            infixField.setText(infixField.getText() + " + ");
            break;
            case "-":
            infixField.setText(infixField.getText() + " - ");
            break;
            case "*":
            infixField.setText(infixField.getText() + " * ");
            break;
            case "/":
            infixField.setText(infixField.getText() + " / ");
            break;
            case "^":
            infixField.setText(infixField.getText() + " ^ ");
            break;
            case "(":
            infixField.setText(infixField.getText() + "( ");
            break;
            case ")":
            infixField.setText(infixField.getText() + " ) ");
            break;
            
            case "Pi":
            infixField.setText(infixField.getText() + Math.PI);
            break;
            case "Tan":
            infixField.setText(infixField.getText() + "Tan ( ");
            break;
            case "Cos":
            infixField.setText(infixField.getText() + "Cos ( ");
            break;
            case "Sin":
            infixField.setText(infixField.getText() + "Sin ( ");
            break;
            
        }
    }
}