import java.util.ArrayList;

public class SY
{
    public ArrayList<String> convert(String expr)
    {
        String[] tokens = expr.split(" +");
        ArrayList<String> result = new ArrayList<String>();
        Stack<String> stack = new Stack<String>();
        
        for (String token : tokens)
        {
            if (prec(token) != -1)  // It's an operator!
            {
                if (stack.isEmpty())
                {
                    stack.push(token);
                }
                else if (prec(token) == 1)
                {
                    stack.push(token);
                }
                else if (prec(token) == 2)
                {
                    while(prec(stack.peek()) > 1 )
                    {
                        result.add(stack.pop());
                    }
                    stack.pop();
                }
                else if (prec(stack.peek()) < prec(token) || (prec(stack.peek()) == 5 && prec(token) == 5))
                {
                    stack.push(token);
                }
                else
                {
                    while (!stack.isEmpty() && prec(stack.peek()) >= prec(token))
                    {
                        result.add(stack.pop());
                    }
                    stack.push(token);
                }
            }
            else  // Must be a number
            {
                if(token.equals("Pi") || token.equals("pi") || token.equals("PI"))
                {
                    result.add(Math.PI + "");
                }
                else
                {
                    result.add(token);
                }
            }
            
        }
        
        while (!stack.isEmpty())
        {
            result.add(stack.pop());
        }

        return result;
    }
    
    
    private int prec(String op)
    {
        switch(op)
        { 
            
            case "(":
            return 1;
            
            case ")":
            return 2;
            
            case "+":
            case "-":
            return 3;
            
            case "*":
            case "/":
            return 4;
            
            case "^":
            return 5;
            
            case "Tan":
            case "Cos":
            case "Sin":
            return 6;
            
            default:
            return -1;
        }
    }
}