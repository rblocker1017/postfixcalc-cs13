import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class FullTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class FullTest
{
    /**
     * Default constructor for test class FullTest
     */
    public FullTest()
    {
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp()
    {
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown()
    {
    }
    
    @Test
    public void testAll()
    {
        SY s = new SY();
        Postfix p = new Postfix();
        assertEquals(5.0, p.eval(s.convert("2 + 3")), 0.001);
    }
}